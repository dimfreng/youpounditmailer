class UserController < ApplicationController
	def send_notif
		user = params[:user]
		
		 UserMailer.welcome_email(user).deliver_now
		 render json: {:status => "success"}, status: :created
	end
end
