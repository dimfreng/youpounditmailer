class UserMailer < ApplicationMailer
	#default from: 'notifications@example.com'
 	default from: 'youpoundit2015@gmail.com'

  def welcome_email(user)
    @user = user
    @url  = 'http://example.com/login'
    mail(to: @user[:email], subject: "YouPoundIt notify you.")
  end
end
