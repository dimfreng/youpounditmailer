class ApplicationMailer < ActionMailer::Base
  default from: "youpoundit2015@gmail.com"
  layout 'mailer'
end
